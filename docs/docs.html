<html>
<body>

<h1>How to use pyDoubles?</h1>

<h2>Import the framework in your tests</h2>
<pre lang="python">
import unittest
from pyDoubles.framework import *
</pre>
If you are afraid of importing everything from the pyDoubles.framework
module, you can use custom imports, although it has been carefully
designed to not conflict with your own classes.
<pre lang="python">
import unittest
from pyDoubles.framework import stub, spy, mock, 
from pyDoubles.framework import when, expect_call, assert_that_was_called
from pyDoubles.framework import method_returning, method_raising
</pre>

<h2>Which doubles do you need?</h2>
You can choose to stub out a method in a regular object instance,
to stub the whole object, to create three types of spies and two types of
mock objects.

<h3>Stubs</h3>
There are several ways to stub out methods.

<h4>Stub out a single method</h4>
If you just need to replace a single method in the collaborator
object and you don't care about the input parameters, you can stub out 
just that single method:

<pre lang="python">
collaborator = Collaborator() # create the actual object
collaborator.some_calculation = method_returning(10)
</pre>
Now, when your production code invokes the method "some_calculation" 
in the collaborator object, the framework will return 10, no matter
what parameters are passed in as the input.
<br></br>
If you want the method to raise an exception when called use this:
<pre lang="python">
collaborator.some_calculation = method_raising(ApplicationException())
</pre>
You can pass in any type of exception.
 

<h4>Stub out the whole object</h4>
Now the collaborator instance won't be the actual object but a 
replacement.
<pre lang="python">
collaborator = stub(Collaborator())
</pre>
Any method will return "None" when called with any input parameters.
If you want to change the return value you can use the "when" sentence:
<pre lang="python">
when(collaborator.some_calculation).then_return(10)
</pre>
Now, when your production code invokes "some_calculation" method, the 
stub will return 10, no matter what arguments are passed in.
You can also specify different return values depending on the input:
<pre lang="python">
when(collaborator.some_calculation).with_args(5).then_return(10)
when(collaborator.some_calculation).with_args(10).then_return(20)
</pre>
This means that "collaborator.some_calculation(5)" will return 10,
and that it will return 20 when the input is 10. You can define
as many input/output specifications as you want. Bear in mind 
that defining the return value without the input arguments, will override
any defined specification based on input:
<pre lang="python">
when(collaborator.some_calculation).with_args(5).then_return(10)
when(collaborator.some_calculation).then_return(20)
</pre>
This time, "collaborator.some_calculation(5) will return 20.

<h4>Any argument matches</h4>
The special keyword ANY_ARG is a wildcard for any argument in the
stubbed method:
<pre lang="python">
when(collaborator.some_other_method).with_args(
                    5, ANY_ARG).then_return(10)
</pre>
The method "some_other_method" will return 10 as long as the
first parameter be 5, no matter what the second parameter is.
You can use any combination of "ANY_ARG" arguments. But remember
that, if all of them are ANY, you better don't specify the arguments,
just use this:
<pre lang="python">
when(collaborator.some_other_method).then_return(10)
</pre>

 
 


<h4>Stub out the whole unexisting object</h4>
If the Collaborator class does not exists yet, or you don't
want the framework to check that the call to the
stub object method, matches the actual API in the actual object, you
can use an "empty" stub.
<pre lang="python">
collaborator = empty_stub()
when(collaborator.alpha_operation).then_return("whatever")
</pre>
The framework is creating the method "alpha_operation" dynamically
and making it return "whatever" always for you.
<br></br>
The use of empty_stub, empty_spy or empty_mock is not recommended
because you loose the API match check. We only use them as the
construction of the object is too complex among other circumstances.



<h3>Spies</h3>
Please read the documentation above about stubs, because the API to
define method behaviors is the same for stubs and spies. To create 
the object: 
<pre lang="python">
collaborator = spy(Collaborator())
</pre>
After the execution of the system under test, we want to validate
that certain call was made:
<pre lang="python">
assert_that_was_called(collaborator.send_email)
</pre>
That will make the test pass if method "send_email" was invoked 
one or more times, no matter what arguments were passed in.
We can also be precise about the arguments:
<pre lang="python">
assert_that_was_called(
   collaborator.send_email).with_args("example@iexpertos.com")
</pre>
Notice that you can combine the "when" statement with 
the called assertion:

<pre lang="python">
def test_sut_asks_the_collaborator_to_send_the_email(self):
   sender = spy(Sender())
   when(sender.send_email).then_return(SUCCESS)
   object_under_test = Sut(sender)

   object_under_test.some_action()

   assert_that_was_called(
      sender.send_email).with_args("example@iexpertos.com")
</pre>
Any other call to any method in the "sender" double will
return "None" and will not interrupt the test. We are 
not telling all that happens between the sender and the
SUT, we are just asserting on what we want to verify.<br></br>
The ANY_ARG matcher can be used to verify the call as well:
<pre lang="python">
assert_that_was_called(
      collaborator.some_other_method).with_args(5, ANY_ARG)
</pre>
<br></br>

You can also create an "empty_spy" to not base the object in a
certain instance:
<pre lang="python">
sender = empty_spy()
</pre>

<h4>The ProxySpy</h4>
There is a special type of spy supported by the framework which
is the ProxySpy:
<pre lang="python">
collaborator = proxy_spy(Collaborator())
</pre>
The proxy spy will record any call made to the object but rather
than replacing the actual methods in the actual object, it will
execute them. So the actual methods in the Collaborator will 
be invoked by default. You can replace the methods
one by one using the "when" statement:
<pre lang="python">
when(collaborator.some_calculation).then_return(1000)
</pre>
Now "some_calculation" method will be a stub method but the
remaining methods in the class will be for real.<br></br>

The ProxySpy might be interesting when you don't know what
the actual method will return in a given scenario but still
you want to check that some call is made. It can be used
even for debugging purposes. 








<h3>Mocks</h3>
Before calls are made, they have to be expected:
<pre lang="python">
def test_sut_asks_the_collaborator_to_send_the_email(self):
   sender = mock(Sender())
   expect_call(sender.send_email)
   object_under_test = Sut(sender)

   object_under_test.some_action()

   sender.assert_that_is_satisfied()
</pre>
The test is quite similar to the one using a spy. However the
framework behaves different. If any other call to the sender
is made during "some_action", the test will fail. This makes
the test more fragile. However, it makes sure that 
this interaction is the only one between the two objects, and this
might be important for you.
<br></br>

<h4>More precise expectations</h4>
You can also expect the call to have certain input parameters:
<pre lang="python">
expect_call(sender.send_email).with_args("example@iexpertos.com")
</pre>


<h4>Setting the return of the expected call</h4>
Additionally, if you want to return anything when the expected call 
occurs, there are two ways:
<pre lang="python">
expect_call(sender.send_email).returning(SUCCESS)
</pre>
Which will return SUCCESS whatever arguments you pass in, or
<pre lang="python">
expect_call(sender.send_email).with_args(
                 "wrong_email").returning(FAILURE)
</pre>
Which expects the method to be invoked with "wrong_email"
and will return FAILURE. 
<br></br>
As you might have seen, the "when" statement is not used for
mocks, only for stubs and spies. Mock objects use the 
"expect_call" syntax together with the "assert_that_is_satisfied" 
(instance method).

<h2>More documentation</h2>
The best and most updated documentation are the unit tests of
the framework itself. We encourage the user to read the tests
and see what features are supported in every commit into the
source code repository.

